defmodule Astrid.DeviceRegistry do
  use GenServer

  ## Client API
  def start_link(opts) do
    GenServer.start_link(__MODULE__, :ok, opts)
  end

  def lookup(server, name) do
    GenServer.call(server, {:lookup, name})
  end

  def add(server, name, ip, info) do
    GenServer.cast(server, {:add, name, ip, info})
  end

  ## Server API
  def init(:ok) do
    {:ok, %{}}
  end

  def handle_call({:lookup, name}, _from, devices) do
    {:reply, Map.fetch(devices, name), devices}
  end

  def handle_cast({:add, name, ip, info}, devices) do
    if Map.has_key?(devices, name) do
      {:noreply, devices}
    else
      {:noreply, Map.put(devices, name, %{ip: ip, info: info})}
    end
  end


end
