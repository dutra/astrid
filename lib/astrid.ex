defmodule Astrid do
  @moduledoc """
  Documentation for Astrid.
  """
  use Application

  def start(_type, _args) do
    Astrid.Supervisor.start_link(name: Astrid.Supervisor)
  end

  @doc """
  Hello world.

  ## Examples

      iex> Astrid.hello
      :world

  """
  def hello do
    :world
  end
end
