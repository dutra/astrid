defmodule Astrid.SmartPlug do
  use GenServer
  use Bitwise

  ## Client API
  def start_link(opts) do
    GenServer.start_link(__MODULE__, :ok, opts)
  end

  def discover(server) do
    GenServer.cast(server, {:discover})
  end

  def on(server, name) do
    GenServer.cast(server, {:on, name})
  end

  def off(server, name) do
    GenServer.cast(server, {:off, name})
  end

  def stop(server) do
    GenServer.stop(server)
  end

  ## Server API
  def init(:ok) do
    {:ok, %{}}
  end

  def handle_cast({:on, name}, state) do
    packet = encrypt(%{system: %{set_relay_state: %{state: 1}}}, [:header])
    {:ok, %{ip: ip, info: _info}} = Astrid.DeviceRegistry.lookup(Astrid.DeviceRegistry, name)
    IO.inspect ip

    {:ok, socket} = :gen_tcp.connect(ip, 9999,
      [:binary, reuseaddr: true, active: true], 10_000)
    :ok = :gen_tcp.send(socket, packet)
    :ok = :gen_tcp.shutdown(socket, :write)

    {:noreply, state}
  end

  def handle_cast({:off, name}, state) do
    packet = encrypt(%{system: %{set_relay_state: %{state: 0}}}, [:header])
    {:ok, %{ip: ip, info: _info}} = Astrid.DeviceRegistry.lookup(Astrid.DeviceRegistry, name)
    IO.inspect ip
    {:ok, socket} = :gen_tcp.connect(ip, 9999,
      [:binary, reuseaddr: true, active: true, port: 4096], 10_000)
    :ok = :gen_tcp.send(socket, packet)
    :ok = :gen_tcp.shutdown(socket, :write)
    {:noreply, state}
  end

  def handle_cast({:discover}, state) do
    msg = encrypt(%{system: %{get_sysinfo: %{}}})

    if Map.has_key?(state, :udp_socket) do
      udp_socket = Map.fetch!(state, :udp_socket)
      :ok = :gen_udp.send(udp_socket, {255, 255, 255, 255}, 9999, msg)

      {:noreply, state}
    else
      {:ok, udp_socket} = :gen_udp.open(4096,
        [:binary, active: true, broadcast: true, reuseaddr: true])
      :ok = :gen_udp.send(udp_socket, {255, 255, 255, 255}, 9999, msg)
      {:noreply, Map.put(state, :udp_socket, udp_socket)}
    end
  end

  def handle_info({:udp, _udpsocket, ip, _port, packet}, state) do
    info = decrypt(packet)
    name = info["system"]["get_sysinfo"]["alias"]
    Astrid.DeviceRegistry.add(Astrid.DeviceRegistry, name, ip, info)
    IO.puts("Receiving Packets")
    IO.inspect ip
    IO.puts name

    # IO.inspect decrypt(packet)

    {:noreply, state}
  end

  def handle_info({:tcp, _port, packet}, state) do
    IO.puts("Receiving Packets")
    info = decrypt(packet, [:header])
    IO.inspect info

    {:noreply, state}
  end

  def handle_info({:tcp_closed, port}, state) do
    IO.puts "TCP Closed"
    IO.inspect port
    {:noreply, state}
  end

  def handle_info(info, state) do
    IO.inspect info

    {:noreply, state}
  end

  def terminate(_reason, state) do
    if Map.has_key?(state, :udp_socket) do
      udp_socket = Map.fetch!(state, :udp_socket)
      :gen_udp.close(udp_socket)
    end
  end

  defp encrypt(packet, [:header]) do
    string = Poison.encode!(packet)
    charlist = String.to_charlist(string)

    <<length(charlist) :: size(32)>> <> encrypt(packet)
  end

  defp encrypt(packet) do
    string = Poison.encode!(packet)
    charlist = String.to_charlist(string)

    key = 171
    result = []
    {_, encoded} = charlist |>
      Enum.reduce(
        {key, result},
        fn(i, {k, l}) ->
          a = bxor(k, i)
          {a, l ++ [a]}
        end)
    IO.inspect(charlist)
    IO.inspect encoded
    :binary.list_to_bin(encoded)
  end

  defp decrypt(packet, [:header]) do
    <<_ :: size(32)>> <> data = packet
    decrypt(data)
  end

  defp decrypt(packet) do
    key = 171
    result = ''
    charlist = :binary.bin_to_list(packet)

    {_, decoded} = charlist |>
      Enum.reduce(
        {key, result},
        fn(i, {k, l}) ->
          a = bxor(k, i)
          {i, l ++ [a]}
        end)
    Poison.decode!(decoded)
  end

end
