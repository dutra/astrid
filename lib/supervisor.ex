defmodule Astrid.Supervisor do
  use Supervisor

  def start_link(opts) do
    Supervisor.start_link(__MODULE__, :ok, opts)
  end

  def init(:ok) do
    children = [
      {Astrid.DeviceRegistry, name: Astrid.DeviceRegistry},
      {Astrid.SmartPlug, name: Astrid.SmartPlug}
    ]

    Supervisor.init(children, strategy: :one_for_one)

  end
end
